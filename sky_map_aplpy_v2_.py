import numpy as np
import matplotlib.pyplot as pl
import aplpy
from astropy.coordinates import SkyCoord
from astropy import units as u
import glob

pl.rcParams["font.family"] = "Times New Roman"
pl.rcParams['figure.figsize'] =8, 8
pl.rcParams['ytick.minor.visible'] =True
pl.rcParams['xtick.minor.visible'] = True
pl.rcParams['xtick.top'] = True
pl.rcParams['ytick.right'] = True
pl.rcParams['font.size'] = '20'
pl.rcParams['legend.fontsize'] = '13'
pl.rcParams['legend.borderaxespad'] = '1.9'
#pl.rcParams['legend.numpoints'] = '1'
pl.rcParams['figure.titlesize'] = 'medium'
pl.rcParams['figure.titlesize'] = 'medium'
pl.rcParams['xtick.major.size'] = '10'
pl.rcParams['xtick.minor.size'] = '6'
pl.rcParams['xtick.major.width'] = '2'
pl.rcParams['xtick.minor.width'] = '1'
pl.rcParams['ytick.major.size'] = '10'
pl.rcParams['ytick.minor.size'] = '6'
pl.rcParams['ytick.major.width'] = '2'
pl.rcParams['ytick.minor.width'] = '1'
pl.rcParams['xtick.direction'] = 'in'
pl.rcParams['ytick.direction'] = 'in'
pl.rcParams['axes.labelpad'] = '10.0'
pl.rcParams['lines.dashed_pattern']=3.0, 1.4
#pl.rcParams['axes.formatter.limits']=-10,10
pl.rcParams['lines.dotted_pattern']= 1.0, 0.7

pl.rcParams['xtick.labelsize'] = '16'
pl.rcParams['ytick.labelsize'] = '16'
pl.rcParams['axes.labelsize'] = '16'
pl.rcParams['axes.labelsize'] = '16'
pl.rcParams['axes.labelweight'] = 'bold'

pl.rcParams['xtick.major.pad']='10'
pl.rcParams['xtick.minor.pad']='10'
pl.rcParams['hatch.color'] = 'black'
pl.rc('axes', linewidth=2)

#######################################
files=[]
for i in glob.glob('*.fits'):
    b = aplpy.FITSFigure(i) # Fits image name
    b.show_colorscale(cmap='jet',aspect='equal',vmid=None, stretch='linear', smooth=None, kernel='gauss', interpolation='bessel') 
    b.frame.set_linewidth(1)
    b.axis_labels.set_xtext('Right Ascension (J2000)')
    b.axis_labels.set_ytext('Declination (J2000)') 
    b.axis_labels.set_font(size='15')
    b.tick_labels.set_xformat('dd')
    b.tick_labels.set_yformat('dd:mm')
    b.tick_labels.set_font(size='medium', weight='medium', stretch='normal', family='sans-serif’',style='normal', variant='normal')
    b.ticks.set_color('black')
    b.ticks.set_tick_direction('out')
    b.add_colorbar()
    b.colorbar.set_axis_label_text('Jy/beam')
    b.colorbar.set_axis_label_font(size='medium', weight='medium',stretch='normal', family='Times New Roman',style='normal', variant='normal')
    #b.colorbar.set_location('right')
    #b.set_theme('publication')
    b.save(str(i)+".pdf",dpi=300,transparent=False, adjust_bbox=True, max_dpi=300, format='pdf')