import numpy as np
import scipy
from scipy import signal
import pylab as pl
from mpl_toolkits.axes_grid1 import ImageGrid
import matplotlib.pyplot as plt
from astropy.io import fits
from astropy.visualization import astropy_mpl_style
from astropy.utils.data import get_pkg_data_filename
from astropy.wcs import WCS
import os
from astropy import units as u
import shutil
from pyfiglet import Figlet as f

x=f(font='slant')
print(x.renderText('KOTHA KOM KAJ BESI'))

plt.rcParams["font.family"] = "Serif"
plt.rcParams['ytick.minor.visible'] =True
plt.rcParams['xtick.minor.visible'] = True
plt.rcParams['axes.linewidth'] = 3.5
plt.rcParams['xtick.major.width'] = 3
plt.rcParams['ytick.major.width'] = 3
plt.rcParams['xtick.minor.width'] = 2
plt.rcParams['ytick.minor.width'] = 2

plt.rcParams['axes.linewidth'] = 3
plt.rcParams['xtick.major.width'] = 3
plt.rcParams['ytick.major.width'] = 3
plt.rcParams['xtick.minor.width'] = 2
plt.rcParams['ytick.minor.width'] = 2
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif']='Times New Roman'
plt.rcParams['legend.fontsize'] = 32

plt.rcParams['axes.linewidth'] = 3.5
plt.rcParams['axes.labelpad'] = '15.0'
# plt.rcParams['axes.labelweight'] = 'bold'       # weight of the x and y labels


plt.rcParams['xtick.major.size']=15
plt.rcParams['ytick.major.size']=15
plt.rcParams['xtick.minor.size']=10
plt.rcParams['ytick.minor.size']=10
plt.rcParams['xtick.labelsize']=38
plt.rcParams['ytick.labelsize']=38
#pl.tick_params(labelsize=30)
plt.rcParams['xtick.direction']='out'
plt.rcParams['ytick.direction']='out'
plt.rcParams['xtick.top']=True
plt.rcParams['ytick.right']=True
plt.rcParams['axes.axisbelow']=True
plt.rc('xtick', labelsize=25)
plt.rc('ytick', labelsize=25)

path = str(input("Enter path of the FITS files: "))
os.chdir(path)
d = get_ipython().getoutput('ls *.fits')
os.mkdir('SKA_Maps')


for i in d:
    hdul = fits.open(path+str(i))
    wcs = WCS(hdul[0].header)
    data = hdul[0].data
    s = np.shape(data)
    data1 = data.reshape((s[2],s[3]))
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(1, 1, 1, projection=wcs, slices=('x', 'y', 0, 0,))
    im = ax.imshow(data1,  origin='lower', cmap='turbo',interpolation='gaussian')
    ax.set_xlabel('Right Ascension (J2000)', size = '22')
    ax.set_ylabel('Declination (J2000)', size = '22')
    cb = fig.colorbar(im, extend='both', label ='Jy/beam', fraction = 0.045)
    cb.set_label(label ='Jy/beam', size=22,weight='medium', labelpad=30)
    plt.savefig(str(i)+'.pdf', dpi = 300,bbox_inches='tight',format='pdf')
    shutil.move(path+str(i)+".pdf", path+'/SKA_Maps/'+str(i)+".pdf")



