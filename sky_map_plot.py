import numpy as np
import h5py as h
import matplotlib.pyplot as pl

pl.rcParams['figure.figsize'] = 8, 7
pl.rcParams['ytick.minor.visible'] =True
pl.rcParams['xtick.minor.visible'] = True
pl.rcParams['xtick.top'] = True
pl.rcParams['ytick.right'] = True
pl.rcParams['font.size'] = '20'
pl.rcParams['legend.fontsize'] = '13'
pl.rcParams['legend.borderaxespad'] = '1.9'
#pl.rcParams['legend.numpoints'] = '1'
pl.rcParams['figure.titlesize'] = 'medium'
pl.rcParams['figure.titlesize'] = 'medium'
pl.rcParams['xtick.major.size'] = '10'
pl.rcParams['xtick.minor.size'] = '6'
pl.rcParams['xtick.major.width'] = '2'
pl.rcParams['xtick.minor.width'] = '1'
pl.rcParams['ytick.major.size'] = '10'
pl.rcParams['ytick.minor.size'] = '6'
pl.rcParams['ytick.major.width'] = '2'
pl.rcParams['ytick.minor.width'] = '1'
pl.rcParams['xtick.direction'] = 'in'
pl.rcParams['ytick.direction'] = 'in'
pl.rcParams['axes.labelpad'] = '10.0'
pl.rcParams['lines.dashed_pattern']=3.0, 1.4
#pl.rcParams['axes.formatter.limits']=-10,10
pl.rcParams['lines.dotted_pattern']= 1.0, 0.7

pl.rcParams['xtick.labelsize'] = '16'
pl.rcParams['ytick.labelsize'] = '16'
pl.rcParams['axes.labelsize'] = '16'
pl.rcParams['axes.labelsize'] = '16'
pl.rcParams['axes.labelweight'] = 'bold'

pl.rcParams['xtick.major.pad']='10'
pl.rcParams['xtick.minor.pad']='10'
pl.rcParams['hatch.color'] = 'black'
pl.rc('axes', linewidth=2)

image=h.File("Observed_sky.h5","r")
data=image['data'][:]
image.close()


#################################################################################################################
from mpl_toolkits.axes_grid1 import make_axes_locatable
ax=pl.subplot()
im=ax.imshow(data,cmap='gray',origin='lower',vmin=0.01,vmax=0.1,interpolation='none', extent=[224,226,-31,-29])
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
pl.colorbar(im,cax=cax,label="Jy/beam")
ax.set_xlabel("RA (J2000)")
ax.set_ylabel("DEC (J2000)")
xlabels=[r'$227^{\circ}$',r'$226^{\circ}$',r'$225^{\circ}$',r'$224^{\circ}$',r'$223^{\circ}$',r'$222^{\circ}$']
#xlabels=[r'$224^{\circ}$',r'$225^{\circ}$',r'$226^{\circ}$']
ylabels=[r'$-29^{\circ}$',r'$-30^{\circ}$',r'$-31^{\circ}$']
#ax.get_xticks()
ax.set_xticklabels(xlabels)
ax.set_yticklabels(ylabels)
pl.tight_layout()
pl.show()
